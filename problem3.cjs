const inventory = require("./inventory.cjs");

function problem3(inventory) {

    if (arguments < 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }

    let result = inventory.map((element) => element.car_model)
    return result.sort();
}

module.exports = problem3;