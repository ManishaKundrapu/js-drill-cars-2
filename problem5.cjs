const inventory = require("./inventory.cjs");
const problem4 = require("./problem4.cjs");

const prev = problem4(inventory);

function problem5(prev) {
    if (arguments < 1 || prev.length === 0 || !Array.isArray(prev)) {
        return [];
    }

    let result = inventory.filter((element) => element.car_year < 2000);
    return result.length;
}

module.exports = problem5;
