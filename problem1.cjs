const inventory = require("./inventory.cjs");

// const checkId = 33;

function problem1(inventory, checkId) {

    if (arguments.length !== 2 || inventory.length === 0 || !Array.isArray(inventory) || (checkId < 1 && checkId > 50) || typeof (checkId) != 'number') {
        return [];
    }

    return inventory.filter((val) => {
        return val.id === checkId;
    })[0];

}

module.exports = problem1;
