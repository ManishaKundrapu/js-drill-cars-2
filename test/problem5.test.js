const data = require('../inventory.cjs');
const problem4 = require('../problem4.cjs');
const problem5 = require('../problem5.cjs');

const olderCars = problem5(problem4(data));

test('Testing problem5', () => {
    expect(olderCars).toEqual(25);
});
