const inventory = require("./inventory.cjs");

function problem4(inventory) {
    if (arguments < 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }

    let years = inventory.map((element) => element.car_year)
    return years;
}

module.exports = problem4;
