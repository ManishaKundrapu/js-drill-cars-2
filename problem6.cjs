const inventory = require("./inventory.cjs");

function problem6(inventory) {

    if (arguments < 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }

    let result = inventory.filter((element) => element.car_make === "BMW" || element.car_make === "Audi")

    return JSON.stringify(result);
}

module.exports = problem6;
